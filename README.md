# HTML/CSS S3: Ejercicios

### Ejercicio 1: Grid de MI_TIENDA

Desarrolla el HTML y el CSS de una de las páginas de productos de MI_TIENDA.
Recuerda que además de preocuparte del CSS, debes también darle cariño al HTML y hacer uso de las etiquetas oportunas en cada caso.

Aplica los estilos que has aprendido para intentar que la web se parezca lo máximo posible al diseño, recuerda que puedes hacer uso de:

- Display: block, inline-block (para representar las filas y columnas) [o flex]
- Position absolute (para hacer ese menú [aside izquierdo] tan raro...)
- Paddings / márgenes
- Tamaños con porcentajes (para el ancho de los productos)
